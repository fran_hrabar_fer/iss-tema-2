package org.flightgear.fgfsclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class PropertyToggleButton extends PropertyModifier {

	private JToggleButton value;
	
	public PropertyToggleButton(FGFSConnection fgfs,
								String name,
								String caption) {
		super(fgfs, name, caption);
		
		value = new JToggleButton("ON");
		value.addActionListener(new ActionListener () {
		    public void actionPerformed (ActionEvent ev) {
				try {
				    modify();
				    grabFocus();
				} catch (IOException ex) {
				    System.err.println("Failed to update " + propertyName);
				}
		    }
		});
		
		add(value);
		
	}
	@Override
	public void modify() throws IOException {
		fgfs.setBoolean(propertyName, !value.isSelected());
		if (value.isSelected()) {
            value.setText("OFF");
		}
        else  
            value.setText("ON");  
	}

	@Override
	public void update() throws IOException {
		// TODO mozda ostavi prazno jer se nece samo od sebe mijenjati 
		// (valjda ne koristimo 2 GUI-a u isto vrijeme)
		if (!value.hasFocus()) {
			String state = (fgfs.get(propertyName).equals("true") ? "ON" : "OFF");
		    value.setText(state);
		}
	}

}
