package org.flightgear.fgfsclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.Timer;

public class PropertyOscillateInstrument extends PropertyModifier{

	private JTextField lowerValue, upperValue;
	private JButton start, stop;
	private double instrumentValue;
	private boolean direction;
	private double rate = 10;
	private Timer timer;
	private String serviceable;
	
	public PropertyOscillateInstrument (FGFSConnection fgfs,
										String name,
										String serviceable,
										String caption) {
		super(fgfs, name, caption);
		this.serviceable = serviceable;
		lowerValue = new JTextField(10);
		upperValue = new JTextField(10);
		
//		lowerValue.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				try {
//					if (isNumeric(lowerValue.getText()) && isNumeric(upperValue.getText()))
//						modify();
//					grabFocus();
//				} catch (Exception e2) {
//					System.err.println("Failed to update " + propertyName);
//				}
//				
//			}
//		});
		
		start = new JButton("Start");
		stop = new JButton("Stop");
		
		start.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					if (isNumeric(lowerValue.getText()) && isNumeric(upperValue.getText()))
						modify();
					grabFocus();
				} catch (Exception e2) {
					System.err.println("Failed to update " + propertyName);
				}
			}
		});
		
		stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					stop();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		add(lowerValue);
		add(upperValue);
		add(start);
		add(stop);
	}
	
	public void stop() throws IOException {
		if(timer != null) {
			timer.stop();
			timer = null;
		}
		fgfs.setBoolean(serviceable, true);
	}
	
	@Override
	/*
	 * Zapocinje odskok
	 * */
	public void modify() throws IOException {
		// TODO Auto-generated method stub
		
		fgfs.setBoolean(serviceable, false);
		
		try {  
		    instrumentValue = Double.parseDouble(lowerValue.getText());
		  } catch(NumberFormatException e){
			  System.err.println("Failed to update " + propertyName);
		  }
		direction = true;
		ActionListener periodicTask = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (direction) {
					instrumentValue += rate;
					if(instrumentValue >= Double.parseDouble(upperValue.getText()))
						direction = false;
				} else {
					instrumentValue -= rate;
					if(instrumentValue <= Double.parseDouble(lowerValue.getText()))
						direction = true;
				}
				try {
					fgfs.setDouble(propertyName, instrumentValue);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		};
		timer = new Timer(100, periodicTask);
		timer.setRepeats(true);
		timer.start();
	}

	@Override
	public void update() throws IOException {
		// Nista za pokazat
		
	}
	
	public static boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}

}
