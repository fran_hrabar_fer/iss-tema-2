package org.flightgear.fgfsclient;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class PropertyPage
    extends JPanel
{

    public PropertyPage (FGFSConnection fgfs, String name)
    {
	this.fgfs = fgfs;
	this.name = name;
	fields = new ArrayList();
	setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public String getName ()
    {
	return name;
    }

    public void addField (String name, String caption)
    {
	PropertyField field = new PropertyField(fgfs, name, caption);
	add(field);
	fields.add(field);
    }
    
    public void addButton (String name, String caption) {
    	PropertyToggleButton button = new PropertyToggleButton(fgfs, name, caption);
    	add(button);
    	fields.add(button);
    }

    public void addOscillator (String name, String serviceable, String caption) {
    	PropertyOscillateInstrument oscillator = new PropertyOscillateInstrument(fgfs, name, serviceable, caption);
    	add(oscillator);
    	fields.add(oscillator);
    }
    public void update ()
	throws IOException
    {
	Iterator it = fields.iterator();
	while (it.hasNext()) {
	    ((PropertyModifier)it.next()).update();
	}
    }

    private FGFSConnection fgfs;
    private String name;
    private ArrayList fields;

}
