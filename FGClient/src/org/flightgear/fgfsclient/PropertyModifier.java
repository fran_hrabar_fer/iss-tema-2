package org.flightgear.fgfsclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public abstract class PropertyModifier extends JPanel {
	protected FGFSConnection fgfs;
    protected String propertyName;
    protected JLabel label;

    public PropertyModifier(FGFSConnection fgfs,
			  						String name,
			  						String caption) {
		this.fgfs = fgfs;
		propertyName = name;
		label = new JLabel(caption);
		add(label);

    }
    
    abstract public void modify() throws IOException;
    abstract public void update() throws IOException;
    
}
