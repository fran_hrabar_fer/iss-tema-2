var delay = 20;
var interval = 5;
var inst_refresh = 0;

turn_off = func(prop) {
	setprop(prop, "false");
	#var message = prop.concat(" off");
	var message = string.join(" ",[prop, " off"]);
    print(message); # prints the message to the black console/terminal window
	gui.popupTip(message, 5);
}

## biti ce tesko posto ne znam kako napraviti staticku varijablu
go_wild1 = func(prop, from, to, step) {
	var val = getprop(prop);
	val = val + step;
	if (val < from) { val = from; }
	else if (val > to) { val = from; }
	setprop(prop, val);
}

go_wild = func(prop, from, to, step) {
	var val = getprop(prop);
	var state_var = string.join("-",[prop, "rising"]);
	var direction = getprop(state_var);
	if(!direction) {
		print("Stvaram");
		setprop(state_var, 1);
		direction = 1;
	}

	val = val + direction*step;
	if (val < from) {
		val = from;
		setprop(state_var, 1);
	}
	else if (val > to) {
		val = to;
		setprop(state_var, -1);
	}
	setprop(prop, val);
}

## Instrumentation failure

var alt_off = func {
	setprop("/instrumentation/altimeter/serviceable", "false");
	var message = "Altimeter off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

alt_off = func() { turn_off("/instrumentation/altimeter/serviceable"); }
alt_wild = func() { go_wild("/instrumentation/altimeter/indicated-altitude-ft", 100, 600, 5); }

settimer(alt_off, delay);
var alt_timer = maketimer(inst_refresh, alt_wild);
alt_timer.start();
delay = delay + interval;

heading_off = func {
	setprop("/instrumentation/heading-indicator/serviceable", "false");
	var message = "heading off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

heading_off = func() { turn_off("/instrumentation/heading-indicator/serviceable"); }
heading_wild = func() { go_wild("/instrumentation/heading-indicator/offset-deg", 50, 300, 2); }

settimer(heading_off, delay);
var heading_timer = maketimer(inst_refresh, heading_wild);
heading_timer.start();
delay = delay + interval;

vertical_speed_off = func {
	setprop("/instrumentation/vertical-speed-indicator/serviceable", "false");
	var message = "vertical_speed off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

vertical_speed_off = func() { turn_off("/instrumentation/vertical-speed-indicator/serviceable"); }
vertical_speed_wild = func() { go_wild("/instrumentation/vertical-speed-indicator/indicated-speed-fpm", 50, 500, 10); }

settimer(vertical_speed_off, delay);
var ver_speed_timer = maketimer(inst_refresh, vertical_speed_wild);
ver_speed_timer.start();
delay = delay + interval;

compass_off = func {
	setprop("/instrumentation/magnetic-compass/serviceable", "false");
	var message = "compass off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

compass_off = func() { turn_off("/instrumentation/magnetic-compass/serviceable"); }

settimer(compass_off, delay);
delay = delay + interval;

slip_off = func {
	setprop("/instrumentation/slip-skid-ball/serviceable", "false");
	var message = "slip skid off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

slip_off = func() { turn_off("/instrumentation/slip-skid-ball/serviceable"); }
slip_wild = func() { go_wild("/instrumentation/magnetic-compass/indicated-slip-skid", 0, 500, 10); }

settimer(slip_off, delay);
var slip_timer = maketimer(inst_refresh, slip_wild);
slip_timer.start();
delay = delay + interval;

adf_off = func() { turn_off("/instrumentation/adf/serviceable"); }
adf_wild = func() { go_wild("/instrumentation/adf/indicated-bearing-deg", 0, 500, 10); }

settimer(adf_off, delay);
var adf_timer = maketimer(inst_refresh, adf_wild);
adf_timer.start();
delay = delay + interval;

dme_off = func() { turn_off("/instrumentation/dme/serviceable"); }
dme_time_wild = func() { go_wild("/instrumentation/dme/indicated-time-min", 0, 60, 10); }
dme_dist_wild = func() { go_wild("/instrumentation/dme/indicated-distance-nm", 0, 200, 10); }
dme_spid_wild = func() { go_wild("/instrumentation/dme/indicated-ground-speed-kt", 0, 200, 10); }

settimer(dme_off, delay);
var dme_time_timer = maketimer(inst_refresh, dme_time_wild);
var dme_dist_timer = maketimer(inst_refresh, dme_dist_wild);
var dme_spid_timer = maketimer(inst_refresh, dme_spid_wild);
dme_time_timer.start();
dme_dist_timer.start();
dme_spid_timer.start();
delay = delay + interval;

cdi_off = func() { turn_off("/instrumentation/nav/cdi/serviceable"); }
cdi_wild = func() { go_wild("/instrumentation/nav/filtered-cdiNAV0-deflection", 0, 60, 10); }

settimer(cdi_off, delay);
var cdi_timer = maketimer(inst_refresh, cdi_wild);
cdi_timer.start();
delay = delay + interval;

gs_off = func() { turn_off("/instrumentation/nav/gs/serviceable"); }
gs_wild = func() { go_wild("/instrumentation/nav/filtered-gsNAV0-deflection", 0, 60, 10); }

settimer(gs_off, delay);
var gs_timer = maketimer(inst_refresh, gs_wild);
gs_timer.start();
delay = delay + interval;

air_speed_off = func() { turn_off("/instrumentation/airspeed-indicator/serviceable"); }
air_speed_wild = func() { go_wild("/instrumentation/airspeed-indicator/indicated-speed-kt", 0, 200, 10); }

settimer(air_speed_off, delay);
var air_speed_timer = maketimer(inst_refresh, air_speed_wild);
air_speed_timer.start();
delay = delay + interval;

turn_ind_off = func() { turn_off("/instrumentation/turn-indicator/serviceable"); }
turn_ind_wild = func() { go_wild("/instrumentation/turn-indicator/indicated-turn-rate", 0, 200, 10); }

settimer(turn_ind_off, delay);
var turn_ind_timer = maketimer(inst_refresh, turn_ind_wild);
turn_ind_timer.start();
delay = delay + interval;

attid_ind_off = func() { turn_off("/instrumentation/attitude-indicator/serviceable"); }
attid_ind_pitch_wild = func() { go_wild("/instrumentation/attitude-indicator/indicated-pitch-deg", 0, 200, 10); }
attid_ind_roll_wild = func() { go_wild("/instrumentation/attitude-indicator/indicated-roll-deg", 0, 200, 10); }

settimer(attid_ind_off, delay);
var attid_ind_pitch_timer = maketimer(inst_refresh, attid_ind_pitch_wild);
var attid_ind_roll_timer = maketimer(inst_refresh, attid_ind_roll_wild);
attid_ind_pitch_timer.start();
attid_ind_roll_timer.start();
delay = delay + interval;

## System failuer

lights_on = func {
	setprop("/controls/electric/external-power", "true");
	setprop("/systems/pitot/serviceable", "true");
	var message = "Lights on";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

settimer(lights_on, delay);
delay = delay + interval;

lights_off = func {
	setprop("/controls/electric/external-power", "false");
	setprop("/systems/pitot/serviceable", "false");
	var message = "Lights off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

lights_off = func() { turn_off("/controls/electric/external-power"); }

settimer(lights_off, delay);
delay = delay + interval;

engine_off = func {
	setprop("/engines/active-engine/kill-engine", "false");
	print("engine_off");
	var message = "engine_off";
    print(message); # prints the message to the black console/terminal window
    gui.popupTip(message, 5);
}

engine_off = func() { turn_off("/engines/active-engine/kill-engine"); }

settimer(engine_off, delay);
delay = delay + interval;

turn_off = func(prop) {
	setprop(prop, "false");
	#var message = prop.concat(" off");
	var message = string.join(" ",[prop, " off"]);
    print(message); # prints the message to the black console/terminal window
	gui.popupTip(message, 5);
}

electrical_off = func() { turn_off("/systems/electrical/serviceable"); }

settimer(electrical_off, delay);
delay = delay + interval;

pilot_off = func() { turn_off("/systems/pitot/serviceable"); }

settimer(pilot_off, delay);
delay = delay + interval;

static_off = func() { turn_off("/systems/static/serviceable"); }

settimer(static_off, delay);
delay = delay + interval;

vacuum_off = func() { turn_off("/systems/vacuum/serviceable"); }

settimer(vacuum_off, delay);
delay = delay + interval;

gear_off = func() { turn_off("/gear/serviceable"); }

settimer(gear_off, delay);
delay = delay + interval;

flaps_off = func() { turn_off("/controls/flight/flaps-serviceable"); }

settimer(flaps_off, delay);
delay = delay + interval;
